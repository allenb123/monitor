package main

import (
	"cloud.google.com/go/civil"
	"encoding/json"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"fmt"
	"html/template"
	"io"
	"os"
	"time"
)

func pullData() map[Session]map[string]int {
	ddir, err := os.Open(".data")
	if err != nil {
		log.Println("pull error:", err)
		return make(map[Session]map[string]int)
	}
	defer ddir.Close()

	data := make(map[Session]map[string]int)

	finfos, err := ddir.Readdir(0)
	if err != nil {
		log.Println("pull error:", err)
		return data
	}
	for _, finfo := range finfos {
		if !finfo.IsDir() {
			continue
		}

		pdir, err := os.Open(".data/" + finfo.Name())
		if err != nil {
			log.Println("pull error:", err)
			continue
		}

		names, err := pdir.Readdirnames(0)
		if err != nil {
			log.Println("pull error:", err)
			pdir.Close()
			continue
		}

		for _, name := range names {
			sdata := make(map[string]int)

			body, err := ioutil.ReadFile(".data/" + finfo.Name() + "/" + name)
			if err != nil {
				log.Println("pull error:", err)
				continue
			}

			lines := strings.Split(string(body), "\n")
			for _, line := range lines {
				f := strings.Fields(line)
				if len(f) == 0 {
					continue
				}
				prog := strings.Join(f[:len(f)-1], " ")
				timestr := f[len(f)-1]
				time, err := strconv.Atoi(timestr)
				if err != nil {
					log.Println("pull error:", err)
					continue
				}

				sdata[prog] = time
			}

			date, err := civil.ParseDate(strings.Split(name, ".")[0])
			if err != nil {
				log.Println("pull error:", err)
				continue
			}
			data[Session{finfo.Name(), date}] = sdata
		}

		pdir.Close()
	}

	return data
}

func pushData(data map[Session]map[string]int) {
	os.Mkdir(".data", os.ModeDir|os.ModePerm)
	for session, info := range data {
		os.MkdirAll(".data/"+session.Name, os.ModeDir|os.ModePerm)
		f, err := os.Create(".data/" + session.Name + "/" + session.Date.String() + ".txt")

		if err != nil {
			log.Println("push error:", err)
			continue
		}

		for prog, time := range info {
			io.WriteString(f, prog+" "+fmt.Sprint(time)+"\n")
		}

		f.Close()
	}
}

type Session struct {
	Name string
	Date civil.Date
}

type AddMessage struct {
	Session Session
	Data    map[string]int
}

type GetResponse struct {
	Data        map[string]int `json:"data"`
	TimeTracked [][2]time.Time `json:"time"`
}

func main() {
	addChan := make(chan AddMessage)
	getChan := make(chan Session)
	getRespChan := make(chan GetResponse, 2)

	go func() {
		nextPush := time.After(60 * time.Second)

		// session id => secs
		data := pullData()
		timeTracked := make(map[Session][][2]time.Time)

		for {
			select {
			case addMsg := <-addChan:
				if data[addMsg.Session] == nil {
					data[addMsg.Session] = make(map[string]int)
				}

				m := data[addMsg.Session]
				total := 0
				for key, val := range addMsg.Data {
					m[key] += val
					total += val
				}

				timeTrackedArr := timeTracked[addMsg.Session]
				timespan := [2]time.Time{time.Now().Add(-time.Duration(total) * time.Second), time.Now()}
				if len(timeTracked[addMsg.Session]) == 0 {
					timeTracked[addMsg.Session] = append(timeTracked[addMsg.Session], timespan)
				} else if timeTrackedArr[len(timeTrackedArr)-1][1].After(timespan[0].Add(-5 * time.Second)) {
					timeTracked[addMsg.Session][len(timeTrackedArr)-1][1] = timespan[1]
				} else {
					timeTracked[addMsg.Session] = append(timeTracked[addMsg.Session], timespan)
				}

			case session := <-getChan:
				cpy := make(map[string]int)
				for key, val := range data[session] {
					cpy[key] = val
				}
				cpyTime := append(([][2]time.Time)(nil), timeTracked[session]...)
				getRespChan <- GetResponse{cpy, cpyTime}

			case <-nextPush:
				pushData(data)
				nextPush = time.After(60 * time.Second)
			}
		}
	}()

	r := mux.NewRouter()
	r.HandleFunc("/{session}/add", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)

		err := r.ParseMultipartForm(1 << 32)
		if err != nil {
			log.Println(err)
			w.WriteHeader(400)
			return
		}

		data := make(map[string]int)
		for program, secstr := range r.PostForm {
			if len(secstr) != 0 {
				data[program], _ = strconv.Atoi(secstr[0])
			}
		}

		session := Session{
			Name: vars["session"],
			Date: civil.DateOf(time.Now()),
		}

		addChan <- AddMessage{session, data}

		w.WriteHeader(200)
	}).Methods("POST")
	r.HandleFunc("/{session}/{date}/data.json", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)

		d, err := time.Parse("2006-01-02", vars["date"])
		if err != nil {
			w.WriteHeader(400)
			w.Write([]byte("Invalid date"))
			return
		}

		getChan <- Session{
			Name: vars["session"],
			Date: civil.DateOf(d),
		}

		data := <-getRespChan
		jsn, err := json.Marshal(data)

		if err != nil {
			log.Println(err)
			w.WriteHeader(500)
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(200)
		w.Write(jsn)
	})

	r.HandleFunc("/session.css", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "session.css")
	})

	r.HandleFunc("/{session}", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		w.Header().Set("Location", "/" + vars["session"] + "/" + time.Now().Format("2006-01-02"))
		w.WriteHeader(302)
	})

	r.HandleFunc("/{session}/{date}", func(w http.ResponseWriter, r *http.Request) {		
		vars := mux.Vars(r)

		d, err := civil.ParseDate(vars["date"])
		if err != nil {
			w.WriteHeader(400)
			return
		}

		getChan <- Session{vars["session"], d}
		data := 		<-getRespChan

		maxSecs := 0
		progOrder := make([]string, 0)
		for prog, secs := range data.Data {
			if secs > maxSecs {
				maxSecs= secs
			}
			progOrder = append(progOrder, prog)
		}

		sort.Slice(progOrder, func (i, j int) bool {
			return data.Data[progOrder[i]] > data.Data[progOrder[j]]
		})

		t, err := template.New("session.tmpl").Funcs(template.FuncMap{
			"duration": func (secs int) time.Duration {
				return time.Duration(secs) * time.Second
			},
			"width": func (secs int, maxWidth int) int {
				return maxWidth * secs / maxSecs
			},
			"sanitize": func (prog string) string {
				return strings.Replace(prog, "_", " ", -1)
			},
			"sub": func(a, b int) int {
				return a - b
			},
		}).ParseFiles("session.tmpl")

		if err != nil {
			log.Println(err)
			w.WriteHeader(500)
			return
		}

		err = t.Execute(w, map[string]interface{}{
			"Name": vars["session"],
			"Data": data,
			"Programs": progOrder,
		})
		if err != nil {
			log.Println(err)
		}
	})

	http.Handle("/", r)

	fmt.Println("http://0.0.0.0:5234/")
	http.ListenAndServe(":5234", nil)
}
