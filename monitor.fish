#!/bin/env fish

source dict.fish

set host $argv[1]
set session $argv[2]

echo "http://$host/$session"

function titlecase
    set out ""
    for word in (string split " " $argv[1] | string split "." | string split "-" | string split "_")
        set out "$out"(string sub -l 1 $word | string upper)
        set out "$out"(string sub -s 2 $word | string lower)" "
    end
    echo (string sub -l (math (string length $out) - 1) $out)
end

function getactiveprogram
    switch (uname)
        case "Darwin"
            set data (osascript getactiveprogram.scpt)
            set progname $data[1]
            set wintitle $data[2]
        case "*"
            set progname (ps -o cmd -p (xdotool getwindowfocus getwindowpid))[2]
            set progname (echo $progname | string split " ")[1]
            set progname (echo $progname | string split "/")[-1]
            set progname (echo $progname | string split ".")[-1]
            set wintitle (xdotool getwindowfocus getwindowname)
    end

    set progname (titlecase $progname)

    if test "$progname" = "Firefox"; or test "$progname" = "Google Chrome"; or test "$progname" = "Chromium"; or test "$progname" = "Safari"; or test "$progname" = "Epiphany"
        if string match -e YouTube "$wintitle" >/dev/null
            echo "YouTube"
        else if string match -e "Google Sheets" "$wintitle" >/dev/null
            echo "Google Sheets"
        else if string match -e "Google Docs" "$wintitle" >/dev/null
            echo "Google Docs"
        else if string match -e "Google Slides" "$wintitle" >/dev/null
            echo "Google Slides"
        else if string match -e " - GoDoc" "$wintitle" >/dev/null
            echo "GoDoc"
        else if string match -e " - Stack Overflow" "$wintitle" >/dev/null; or string match -e "Stack Overflow -" "$wintitle" >/dev/null
            echo "StackOverflow"
        else if string match -e " — Bitbucket" "$wintitle" >/dev/null
            echo "Bitbucket"
        else if string match -e " on Scratch" "$wintitle" >/dev/null
            echo "Scratch"
        else if string match -e "| MDN" "$wintitle" >/dev/null
            echo "MDN Web Docs"
        else
            echo "$progname"
        end
    else if test "$progname" = "Java"
        if string match -e "Minecraft" "$wintitle" >/dev/null; or string match -e "Lunar Client" "$wintitle" >/dev/null
            echo "Minecraft"
        else
            echo "Java"
        end
    else if test "$progname" = ""
        echo "$wintitle"
    else
        echo "$progname"
    end
end

function push
    set list
        
    for key in (dict keys monitorsecs)
        set list $list "$key="(dict get monitorsecs $key)
    end
    curl -X POST (echo '-F '$list | string split " ") "http://$host/$session/add"
        and dict clear monitorsecs
end

set i 0
while true
    set active (getactiveprogram)
    dict set monitorsecs $active (expr (dict get monitorsecs $active) + 1; or echo 1)
    sleep 1

    if test (math $i '%' 15) = 5
        push
    end

    set i (math $i + 1)
end